from __future__ import unicode_literals, absolute_import, print_function

from flask import Blueprint, render_template, request, flash, url_for, redirect
from flask_login import login_user, logout_user
from flask_wtf import Form
from wtforms import fields, validators

from envision.ext import login_manager
from envision.models.user import User


bp = Blueprint('user', __name__)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


@bp.route('/login', methods=['GET', 'POST'])
def login():
    next_url = request.args.get('next', default=url_for('index.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_by_email(form.data['email'])
        if user and user.verify_password(form.data['password']):
            login_user(user)
            flash('Logged in successfully.')
            return redirect(next_url)
        else:
            flash('The user does not exist or the password is wrong.')
    return render_template('login.html', form=form)


@bp.route('/logout', methods=['POST'])
def logout():
    logout_user()
    if request.is_xhr:
        return '', 204
    return redirect(url_for('user.login'))


class LoginForm(Form):
    email = fields.TextField('Email', validators=[
        validators.DataRequired(),
        validators.Email(),
    ])
    password = fields.PasswordField('Password', validators=[
        validators.DataRequired(),
    ])
    submit = fields.SubmitField('Login')
